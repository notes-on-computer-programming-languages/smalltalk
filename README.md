# smalltalk

Object-oriented, dynamically typed reflective programming language.
https://en.m.wikipedia.org/wiki/Smalltalk

## Tutorials
* [Learn X in Y minutes, where X=Smalltalk](https://learnxinyminutes.com/docs/smalltalk/)

## Try it on line
* Not yet!

## free Smalltalk HTTP server: Swazoo
* https://www.gnu.org/software/smalltalk/manual/html_node/Swazoo.html

## Seaside web framework
* https://en.m.wikipedia.org/wiki/Seaside_(software)